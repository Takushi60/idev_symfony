# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Hôte: 127.0.0.1 (MySQL 5.7.19)
# Base de données: idev_symfony
# Temps de génération: 2017-08-05 13:37:30 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Affichage de la table task
# ------------------------------------------------------------

DROP TABLE IF EXISTS `task`;

CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tasklist_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `done` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_527EDB25FF3475DB` (`tasklist_id`),
  CONSTRAINT `FK_527EDB25FF3475DB` FOREIGN KEY (`tasklist_id`) REFERENCES `tasklist` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;

INSERT INTO `task` (`id`, `tasklist_id`, `title`, `done`)
VALUES
	(1,1,'Lait',0),
	(2,1,'Yaourts',0),
	(3,1,'Œufs',0),
	(4,1,'Steaks',0),
	(5,1,'Tomates',0),
	(6,1,'Salade',0),
	(7,2,'Benjamin me doit 2€',0),
	(8,2,'Adrien doit me rendre Zelda',0),
	(9,2,'Christophe me doit un kebab',1),
	(10,2,'Alexandre me doit un ticket restorant',0),
	(11,2,'Je doit 5€ à Thi',0),
	(12,3,'Prendre rdv chez le médecin',0),
	(13,3,'Téléphoner à Philippe',1),
	(14,3,'Répondre à Justine',1),
	(15,3,'Faire les courses',0),
	(16,3,'Faire du sport',0),
	(17,3,'Vérifier les mails',1);

/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table tasklist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tasklist`;

CREATE TABLE `tasklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_662119655E237E06` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `tasklist` WRITE;
/*!40000 ALTER TABLE `tasklist` DISABLE KEYS */;

INSERT INTO `tasklist` (`id`, `name`)
VALUES
	(1,'Courses'),
	(2,'Dettes des amis'),
	(3,'Trucs à faire');

/*!40000 ALTER TABLE `tasklist` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
