idev_symfony
============

# Installation

## 1) Install `node-sass` 4.5.2 globally if not already installed (version 4.5.3 contain bugs)
```bash
sudo npm i --unsafe-perm -g node-sass@4.5.2
```

## 2) Install dependencies and create database
```bash
npm i
composer install
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
mysql -uroot idev_symfony < bdd_dump.sql
```

# Development

## 1) Start servers
```bash
php bin/console server:start && npm run dev
```

# Troubles installation

## Try to update npm
```bash
sudo npm i -g npm
```

## Try to remove `gulp-sass` and re-install it
```bash
rm -Rf node_modules/ package-lock.json
```

> remove line `"gulp-sass": "^2.3.2"` from `package.json`

```bash
npm i -D gulp-sass@2.3.2

# If no error
npm i
```
