const gulp        = require('gulp')
const sass        = require('gulp-sass')
const concat      = require('gulp-concat')
const babel       = require('gulp-babel')
const browserSync = require('browser-sync').create()

gulp.task('sass', () => {
  return gulp.src('src/AppBundle/Assets/styles/app.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('web'))
    .pipe(browserSync.stream())
})

gulp.task('minify', () => {
  return gulp.src('src/AppBundle/Assets/js/*.js')
    .pipe(babel({presets: ['babili']}))
    .pipe(concat('app.js'))
    .pipe(gulp.dest('web'))
    .pipe(browserSync.stream())
})

gulp.task('watch', () => {
  browserSync.init({proxy: 'localhost:8000/app_dev.php'})

  gulp.watch('src/AppBundle/Assets/styles/*.scss', ['sass'])
  gulp.watch('src/AppBundle/Assets/js/*.js', ['minify'])
  gulp.watch('app/Resources/views/**/*.twig').on('change', browserSync.reload)
})