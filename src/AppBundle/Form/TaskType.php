<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class TaskType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $taskListArr = array(
            'class'        => 'AppBundle:TaskList',
            'choice_label' => 'name',
            'label'        => 'Liste'
        );
        if ($options['entityManager'] && $options['defaultChoice']) $taskListArr['data'] = $options['entityManager']->getReference("AppBundle:TaskList", $options['defaultChoice']->getId());

        $builder->add('title', null, array('label' => 'Titre de la tâche'))
                ->add('done', null, array('label' => 'Fait'))
                ->add('tasklist', EntityType::class, $taskListArr);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'    => 'AppBundle\Entity\Task',
            'entityManager' => null,
            'defaultChoice' => 1
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_task';
    }


}
