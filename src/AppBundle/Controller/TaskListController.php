<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TaskList;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tasklist controller
 */
class TaskListController extends Controller
{
    /**
     * List all taskList entities
     * Create taskList entity
     * Remove taskList entity
     *
     * @Route("/", name="list_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $em     = $this->getDoctrine()->getManager();
        $return = array();

        // Get all lists
        $taskLists           = $em->getRepository('AppBundle:TaskList')->findAll();
        $return['taskLists'] = $taskLists;

        // Create delete list form
        $return['deleteListForms'] = array();
        foreach ($taskLists as $list) {
            $deleteForm = $this->createDeleteForm($list);
            $return['deleteListForms'][$list->getId()] = $deleteForm->createView();
        }

        // Create new list form
        $taskList    = new Tasklist();
        $newListForm = $this->createForm('AppBundle\Form\TaskListType', $taskList);
        $newListForm->handleRequest($request);
        $return['newListForm'] = $newListForm->createView();

        // If new list form is submited
        if ($newListForm->isSubmitted() && $newListForm->isValid()) {
            // Check if name already exist
            foreach ($taskLists as $list) {
                if (strtolower($taskList->getName()) === strtolower($list->getName())) {
                    $return['newListFormError'] = 'Ce nom est déjà utilisé.';
                    break;
                }
            }
            // If name is not taken, create list
            if (!isset($return['newListFormError'])) {
                $em->persist($taskList);
                $em->flush();

                return $this->redirectToRoute('list_show', array('id' => $taskList->getId()));
            }
        }

        return $this->render('tasklist/index.html.twig', $return);
    }

    /**
     * Find and displays a taskList entity
     * Edit taskList entity
     *
     * @Route("/list/{id}", name="list_show")
     * @Method({"GET", "POST"})
     */
    public function showAction(TaskList $taskList, Request $request)
    {
        $return = array(
            'taskList' => $taskList,
            'realName' => $taskList->getName()
        );

        // Create edit list form
        $editListForm = $this->createForm('AppBundle\Form\TaskListType', $taskList);
        $editListForm->handleRequest($request);
        $return['editListForm'] = $editListForm->createView();

        if ($editListForm->isSubmitted() && $editListForm->isValid()) {
            $em        = $this->getDoctrine()->getManager();
            $taskLists = $em->getRepository('AppBundle:TaskList')->findAll();
            // Check if name already exist
            foreach ($taskLists as $i => $list) {
                if ($i !== 0 && strtolower($taskList->getName()) === strtolower($list->getName())) {
                    $return['editListFormError'] = 'Ce nom est déjà utilisé';
                    break;
                }
            }
            // If name is not taken, create list
            if (!isset($return['editListFormError'])) {
                $em->flush();
                return $this->redirectToRoute('list_show', array('id' => $taskList->getId()));
            }
        }

        $countTasksDone = 0;
        foreach ($taskList->getTasks() as $task) {
            if ($task->getDone()) $countTasksDone++;
        }
        $return['countTasksDone'] = $countTasksDone;

        return $this->render('tasklist/show.html.twig', $return);
    }

    /**
     * Delete all tasks in taskList entity
     *
     * @Route("/list/deleteall/{id}", name="list_delete_all_tasks")
     * @Method("GET")
     */
    public function deleteAllAction(Request $request, TaskList $taskList)
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($taskList->getTasks() as $task) {
            if ($task->getDone()) $em->remove($task);
        }
        $em->flush();

        return $this->redirectToRoute('list_show', array('id' => $taskList->getId()));
    }

    /**
     * Delete a taskList entity
     *
     * @Route("/list/{id}", name="list_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, TaskList $taskList)
    {
        $form = $this->createDeleteForm($taskList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($taskList);
            $em->flush();
        }

        return $this->redirectToRoute('list_index');
    }

    /**
     * Create a form to delete a taskList entity
     *
     * @param TaskList $taskList The taskList entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TaskList $taskList)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('list_delete', array('id' => $taskList->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}
