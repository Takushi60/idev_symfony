<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Task;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\TaskList;

/**
 * Task controller.
 *
 * @Route("task")
 */
class TaskController extends Controller
{

    /**
     * Creates a new task entity.
     *
     * @Route("/new", name="task_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $return = array();
        if ($request->get('listReferer')) {
            $return['listReferer'] = $this->getDoctrine()->getManager()->getRepository('AppBundle:TaskList')->findById($request->get('listReferer'))[0];
        }

        $task = new Task();
        $return['task'] = $task;

        $form = $this->createForm('AppBundle\Form\TaskType', $task, array('entityManager' => $this->getDoctrine()->getManager(), 'defaultChoice' => $return['listReferer']));
        $form->handleRequest($request);
        $return['form'] = $form->createView();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();

            return $this->redirectToRoute('list_show', array('id' => $task->getTaskList()->getId()));
        }

        return $this->render('task/new.html.twig', $return);
    }

    /**
     * Displays a form to edit an existing task entity.
     *
     * @Route("/{id}/edit", name="task_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Task $task)
    {
        if ($request->get('status') !== null) {
            $task->setDone($request->get('status'));
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('list_show', array('id' => $task->getTaskList()->getId()));
        }
        $editForm = $this->createForm('AppBundle\Form\TaskType', $task);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('list_show', array('id' => $task->getTaskList()->getId()));
        }

        $deleteForm = $this->createDeleteForm($task);

        return $this->render('task/edit.html.twig', array(
            'task'        => $task,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a task entity.
     *
     * @Route("/{id}", name="task_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Task $task)
    {
        $taskList = $task->getTaskList();

        $form = $this->createDeleteForm($task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($task);
            $em->flush();
        }

        return $this->redirectToRoute('list_show', array('id' => $taskList->getId()));
    }

    /**
     * Creates a form to delete a task entity.
     *
     * @param Task $task The task entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Task $task)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('task_delete', array('id' => $task->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
