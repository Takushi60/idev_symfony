/**
 * Open modal when click on delete list
 */
const confirmRemoveList = () => {
  document.querySelectorAll('.js-remove-list').forEach(removeForm => {
    removeForm.addEventListener('submit', e => {
      e.preventDefault()
      const name = removeForm.dataset.listName

      // Clone modal
      const modal = Modal.open('remove_list')

      // Inject list name in modal title
      const text = modal.querySelector('.js-modal-title')
      text.innerHTML = text.innerHTML.replace('#1', name)

      setEventsBtnOnModal(modal, false, () => { removeForm.submit() })
    })
  })
}

/**
 * Open modal when click on delete all tasks in list
 */
const confirmRemoveTasksList = () => {
  document.querySelectorAll('.js-remove-all-tasks').forEach(removeBtn => {
    removeBtn.addEventListener('click', e => {
      e.preventDefault()
      const href = removeBtn.getAttribute('href')
      const name = removeBtn.dataset.listName
      
      // Clone modal
      const modal = Modal.open('remove_all_from_list')

      setEventsBtnOnModal(modal, false, () => { window.location.href = href })
    })
  })
}

/**
 * Open modal when click on delete task
 */
const confirmRemoveTask = () => {
  document.querySelectorAll('.js-remove-task').forEach(removeForm => {
    removeForm.addEventListener('submit', e => {
      e.preventDefault()
      const name = removeForm.dataset.listName

      // Clone modal
      const modal = Modal.open('remove_task')

      setEventsBtnOnModal(modal, false, () => { removeForm.submit() })
    })
  })
}

/**
 * Set events on modal buttons cancel && submit
 * 
 * @param modal DOMHtml modal
 * @param callbackCancel Callback after click on cancel btn
 * @param callbackSubmit Callback after click on submit btn
 */
const setEventsBtnOnModal = (modal, callbackCancel = false, callbackSubmit = false) => {
  const cancelBtn = modal.querySelector('.js-cancel-button')
  const submitBtn = modal.querySelector('.js-submit-button')
  cancelBtn.addEventListener('click', e => {
    Modal.close(modal)
    if (callbackCancel) callbackCancel()
  })
  submitBtn.addEventListener('click', e => {
    Loading.set(true)
    if (callbackSubmit) callbackSubmit()
  })
}

/**
 * Hide / Show edit list form
 */
const toggleEditListForm = () => {
  document.querySelectorAll('.js-edit-list').forEach(editFormBtn => {
    const title    = editFormBtn.parentNode.querySelector('.js-title-rename')
    const btnTexts = editFormBtn.dataset.text.split('|')
    const form     = editFormBtn.parentNode.querySelector('.js-edit-form')
    editFormBtn.addEventListener('click', e => {
      form.classList.toggle('hidden')
      if (form.classList.contains('hidden')) {
        editFormBtn.innerHTML = btnTexts[0]
        title.classList.remove('hidden')
      } else {
        editFormBtn.innerHTML = btnTexts[1]
        title.classList.add('hidden')
      }
    })
    if (editFormBtn.classList.contains('autoclick')) editFormBtn.click()
  })
}

/**
 * Hide / Show tooltips
 */
const initTooltips = () => {
  document.querySelectorAll('[data-tooltip]').forEach(el => {
    const text = el.dataset.tooltip
    el.addEventListener('mouseenter', e => {
      if (!el.querySelector('.tooltip')) {
        let tooltip = document.createElement('div')
        tooltip.classList.add('tooltip')
        tooltip.classList.add('soft-hidden')
        if (el.dataset.tooltipDirection) tooltip.classList.add(`tooltip-${el.dataset.tooltipDirection}`)
        tooltip.innerHTML = text
        el.appendChild(tooltip)
      }
      el.querySelector('.tooltip').classList.remove('soft-hidden')
    })

    el.addEventListener('mouseleave', e => {
      el.querySelector('.tooltip').classList.add('soft-hidden')
    })
  })
}

/**
 * Send form when click on button
 */
const editTaskForm = () => {
  document.querySelectorAll('.js-edit-task-button').forEach(editTaskBtn => {
    const editTaskForm = document.querySelector('.js-edit-task-form')
    editTaskBtn.addEventListener('click', e => {
      e.preventDefault()
      editTaskForm.submit()
    })
  })
}

initTooltips()
confirmRemoveList()
confirmRemoveTasksList()
confirmRemoveTask()
toggleEditListForm()
editTaskForm()