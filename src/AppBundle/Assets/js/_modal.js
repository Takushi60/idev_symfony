const Modal = {
  overlay: document.querySelector('.js-overlay'),
  template: document.querySelector('.js-modal-template'),
  modalOpened: false,

  /**
   * Open modale
   * 
   * @param name modal name attribute
   * 
   * @return Cloned modal DOMElement
   */
  open (name) {
    this.overlay.classList.remove('hidden')
    this.overlay.addEventListener('click', () => { this.close() })

    const modal = this.template.querySelector(`.modal[name="${name}"]`)
    this.modalOpened = document.querySelector('#app').appendChild(modal.cloneNode(true))

    return this.modalOpened
  },

  /**
   * Close modal
   * 
   * @param modal modal DOMElement
   */
  close () {
    this.overlay.classList.add('hidden')
    this.modalOpened.remove()
    this.modalOpened = false
  }

}