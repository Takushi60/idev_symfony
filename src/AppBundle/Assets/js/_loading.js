const Loading = {
  loading: document.querySelector('.js-loading'),

  /**
   * Set loading state
   * 
   * @param state Boolean
   */
  set (state) {
    state ? this.loading.classList.remove('hidden') : this.loading.classList.add('hidden')
  },

  /**
   * Toggle loading state
   */
  toggle () {
    this.loading.classList.contains('hidden') ? this.set(true) : this.set(false)
  }
}